import React from 'react';
import ReactDOM from 'react-dom';
import {Redirect, Router, Route, hashHistory, applyRouterMiddleware} from 'react-router'
import { useScroll } from 'react-router-scroll';


import ComposerList from './components/composers-list';
import PiecesByComposerList from './components/pieces-by-composer-list';
import PieceDetail from './components/piece-detail';
import Autocomplete from './components/autocomplete';

ReactDOM.render((
  <Router
    history={hashHistory}
    render={applyRouterMiddleware(useScroll())}
  >
    <Redirect from="/" to="/composers"/>
    <Route path="/composers" component={ComposerList}/>
    <Route path="/composers/:composer_slug" component={PiecesByComposerList}/>
    <Route path="/pieces/:piece_id" component={PieceDetail}/>
    <Route path="/autocomplete" component={Autocomplete}/>
  </Router>
), document.getElementById('main'))
