require('es6-promise').polyfill();
require('isomorphic-fetch'); // so it works on ios9 and "older" stuff

const rootUrl = 'https://xpto.me/erudit-backend/';
// const rootUrl = 'http://localhost:1212/';

const registry = {};

function fetchJson(path) {
  if ( ! registry[path]) {
    registry[path] = fetch(rootUrl + path)
      .then(response => response.json())
      .catch(() => alert('Error!'));;
  }
  return registry[path];
}

export default {

  queryAutocomplete() {
    return fetchJson('autocomplete');
  },

  queryComposers() {
    return fetchJson('composers');
  },

  queryPerformers() {
    return fetchJson('performers');
  },

  queryPiecesByComposer(composer_slug) {
    return fetchJson(`pieces-by-composer/${composer_slug}`);
  },

  queryPiece(piece_id) {
    return fetchJson(`pieces/${piece_id}`);
  }

};

