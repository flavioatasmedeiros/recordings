import React from 'react';
import {queryAutocomplete} from '../data';
import Typeahead from './typeahead';

export default React.createClass({

  componentDidMount() {
    queryAutocomplete()
      .then(docs => this.setState({docs}));
  },

  render() {
    return this.state ? <div>
      <Typeahead docs={this.state.docs} regainFocus={true}/>
    </div> : null;
  }

});
