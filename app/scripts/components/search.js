import Fuse from 'fuse.js';

export default function(list, input) {
  const options = {
    keys: ['name'],
    threshold: 0.3
  };
  const fuse = new Fuse(list, options);
  const result = fuse.search(input);
  return result;
}
