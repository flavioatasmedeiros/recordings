import React, {PropTypes} from 'react';
import ReactDOM from 'react-dom';
import search from './search';

let searcher, results;

export default React.createClass({

  propTypes: {
    docs: PropTypes.array.isRequired,
    regainFocus: PropTypes.bool
  },

  getDefaultProps() {
    return {
      limit: 10,
      regainFocus: false
    }
  },

  getInitialState() {
    return {
      inputValue: '',
      activeIndex: -1
    }
  },

  componentDidMount() {
    if (this.props.regainFocus) {
      this.focusInput();
    }
  },

  focusInput() {
    const len = this.state.inputValue.length;
    const inputNode = ReactDOM.findDOMNode(this.refs.theInput);
    inputNode.setSelectionRange(len, len);
    inputNode.focus();
  },

  render() {
    return <div className="typeahead">
      <input
        type="text"
        onChange={this.handleChange}
        onKeyDown={this.handleKeyPress}
        value={this.state.inputValue}
        ref="theInput"
      />
      <ul>
        {this.getResults().map(this.renderItem)}
      </ul>
    </div>
  },

  getResults() {
    results = this.state.inputValue.length
      ? search(this.props.docs, this.state.inputValue)
        .slice(0, this.props.limit)
      : [];
    return results;
  },

  renderItem(doc, index) {
    return <li
      className={this.state.activeIndex === index ? 'active' : ''}
      onMouseEnter={() => {
        this.setState({activeIndex: index})
      }}
      onClick={this.handleClick.bind(null, index)}
      key={'typeahead' + doc._id + index}>
        {doc.name} <small>{doc.type}</small>
    </li>
  },

  handleClick(index) {
    this.setState({activeIndex: index}, function() {
        this.handleSelected();
    });
  },


  handleChange(ev) {
    const {value} = ev.currentTarget;
    this.setState({inputValue: value});
  },

  handleKeyPress(ev) {
    let index = this.state.activeIndex;
    const {keyCode} = ev;
    if (38 === keyCode) {
      index--;
    }
    if (40 === keyCode) {
      index++;
    }
    index = index < 0 ? 0 : index;
    index = index > results.length ? results.length : index;
    this.setState({activeIndex: index}, function() {
      if (keyCode === 13) {
        this.handleSelected();
      }
    });
  },

  handleSelected() {
    alert(results[this.state.activeIndex].name);
  }

});
