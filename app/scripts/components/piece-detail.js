import _ from 'lodash';
import uuid from 'uuid';
import React, {PropTypes} from 'react';
import {Link} from 'react-router'
import tools from '../tools';
import {queryPiece} from '../data';

export default React.createClass({

  componentDidMount() {
    queryPiece(this.props.params.piece_id)
      .then(data => this.setState(data));
  },

  render() {
    return this.state ? <div>
      <h1>{this.state.composer.name}</h1>
      <h2>{this.state.name}</h2>
      <br/>
      {this.renderMovements()}
      <br/>
      {this.renderPerformers()}
    </div> : null
  },

  renderMovements() {
    const [recording] = this.state.recordings;
    return this.state.movements.map((item, index) => {
      const track = recording.tracks[index];
      return <p key={`mov${index}`}>
        {tools.toRoman(index + 1)} –
        &nbsp;
        <strong>
          <a href={`https://open.spotify.com/track/${track.spotify_id}`}>
            {item.name}
          </a>
          &nbsp;
          <span>{tools.millisToMinutesAndSeconds(track.duration_ms)}</span>
        </strong>
      </p>
    })
  },

  renderPerformers(performers) {
    const [recording] = this.state.recordings;
    return recording.performers.map((item, index) => {
      return <p key={`perf${index}`}>
        <strong>{item.name}</strong>, {item.role.name}
      </p>
    })
  }

});
