import _ from 'lodash';
import uuid from 'uuid';
import React, {PropTypes} from 'react';
import {Link} from 'react-router'
import {queryComposers} from '../data';

export default React.createClass({

  getInitialState() {
    return {
      docs: []
    }
  },

  componentDidMount() {
    queryComposers()
      .then(docs => this.setState({docs}));
  },

  render() {
    return <div>
      <h2>Composers</h2>
      {this.state.docs.map(doc => <p key={uuid.v4()}>
        <Link to={"composers/" + doc.slug}>{doc.name}</Link>
      </p>)}
    </div>
  }

});
