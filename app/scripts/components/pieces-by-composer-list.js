import _ from 'lodash';
import uuid from 'uuid';
import React, {PropTypes} from 'react';
import {Link} from 'react-router'
import {queryPiecesByComposer} from '../data';

let lastFormat = {slug: null};

export default React.createClass({

  componentDidMount() {
    queryPiecesByComposer(this.props.params.composer_slug)
      .then(data => this.setState(this.prepareData(data)));
  },

  prepareData(data) {
    const sortedPieces = _.sortBy(data.pieces , (piece) => {
      if ( ! piece.format) {
        piece.format = {
          name: 'Other',
          slug: 'other'
        };
        return 'zzz';
      }
      return piece.format.name;
    });
    data.pieces = sortedPieces;
    return data;
  },

  render() {
    return this.state ? <div>
      <h2>{this.state.name}</h2>
      {this.state.pieces.map(piece => <div key={uuid.v4()}>
        {this.renderIfNextFormat(piece)}
        <Link to={"pieces/" + piece.id}>
          {piece.name}
        </Link>
      </div>)}
    </div> : null
  },

  renderIfNextFormat(piece) {
    if (lastFormat.slug != piece.format.slug) {
      lastFormat = piece.format;
      return <div><br/>{piece.format.name}</div>
    }
  }

});
